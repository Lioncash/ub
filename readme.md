# Undefined Behavior

A repository for a document that attempts to document the undefined behavior in versions of the C++ standard.

## Purpose

The C language standard provides, at the end of the document, an annex labeled J. Annex J lists most of the undefined behavior present within the language. This is good, however it has a few issues with it that I don't particularly like, the main one being that it does not provide examples of the undefined behavior being invoked. This sucks for people that learn best through seeing an example first. Nevertheless, this annex is better than being given nothing.

However, in the case of C\+\+, the language standard provides no annex or anything of the sort regarding the undefined behavior in the language. This attempts to be a complement to the C\+\+ standard documents that provides something somewhat like annex J. The entries within the document follow the given pattern

- Title describing an undefined behavior.

- Status of the undefined behavior (which standards it was defined in, and which it was undefined in).

- Quotes from the standard describing the behavior.
  - If necessary due to ambiguity of the quotations, any further justification for the behavior being undefined.

- Any relevant exceptions to the undefined behavior that may not be obvious in the provided quotes.

- Quotes from the standard that allow for the described exceptions, if not present in the initial quotes from the standard. 

- Example(s) of the undefined behavior being invoked.

## Building the Document

1. Have a LaTeX environment installed (as well as pdflatex).
2. Install any relevant packages needed to build the document (see the top of `main.tex` for all packages used).
3. Run `build.py` in the `src` directory, or manually run `pdflatex main` three times in the `src` directory.
4. Done.