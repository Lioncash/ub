\chapter{Expressions}

\begin{UBEntrySection}{A result not being mathematically defined or within the range of representable values for its type during the evaluation of an expression}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr] \P~4}
If during the evaluation of an expression, the result is not
mathematically defined or not in the range of representable values for
its type, the behavior is undefined.
\begin{note}
Treatment of division by zero, forming a remainder using a zero divisor,
and all floating-point exceptions vary among machines, and is sometimes
adjustable by a library function.
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
int k = 0 / 0; // undefined behavior, division by zero not mathematically defined
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Using a reference outside of its lifetime}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr] \P~5}
If an expression initially has the type ``reference to
\tcode{T}''~(11.3.2, 11.6.3) the type is adjusted to
\tcode{T} prior to any further analysis. The expression designates the
object or function denoted by the reference, and the expression
is an lvalue or an xvalue, depending on the expression
\begin{note}
Before the lifetime of the reference has started or after it has ended,
the behavior is undefined (see~6.8).
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
extern int& r1;
int& f();

int& r2 = r1; // undefined behavior, \tcode{r1} not initialized
int& r1 = f();
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Capturing a non-reference entity by reference in a lambda expression and invoking the lambda's function call operator after the entity's lifetime has ended}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.prim.lambda.capture] \P~16}
\begin{note}
If a non-reference entity is implicitly or explicitly captured by reference,
invoking the function call operator of the corresponding \grammarterm{lambda-expression}
after the lifetime of the entity has ended is likely to result in undefined behavior.
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
#include <string>

auto a() {
  std::string str{"Hello"};
  return [&]{ return str.substr(1); };
}

auto b() {
  auto lambda = a();
  return lambda();    // undefined behavior, lambda invoked after \tcode{str}'s limetime has ended
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Calling a function through the wrong function type}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.call] \P~1}
\ldots{}\\
Calling a function through an expression whose function
type is different from the function type of the called
function's definition results in undefined behavior (10.5).\\
\ldots{}
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [expr.reinterpret.cast] \P~6}
A function pointer can be explicitly converted
to a function pointer of a different type.
\begin{note}
The effect of calling a function through a pointer to a function
type~(11.3.5) that is not the same as the type used in the
definition of the function is undefined.
\end{note} \ldots{}
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [dcl.link] \P~1}
\ldots{}\\
Two function types with different language linkages
are distinct types even if they are otherwise identical.\\
\ldots{}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
int fn(int i) {
  return i + 1;
}

int fn2() {
  auto cast_fn = reinterpret_cast<int(*)(double)>(&fn);
  return cast_fn(1); // undefined behavior, call through mismatching function type
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Downcasting a pointer or reference to a class type to a pointer or reference to a derived class type that doesn't actually represent the original class type}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.static.cast] \P~2}
An lvalue of type ``\cvqual{cv1} \tcode{B}'', where \tcode{B} is a class
type, can be cast to type ``reference to \cvqual{cv2} \tcode{D}'', where
\tcode{D} is a class derived~(Clause 13) from \tcode{B},
if \cvqual{cv2} is the
same cv-qualification as, or greater cv-qualification than,
\cvqual{cv1}. If \tcode{B} is a virtual base class of \tcode{D}
or a base class of a virtual base class of \tcode{D},
or if no valid standard conversion from ``pointer to \tcode{D}''
to ``pointer to \tcode{B}'' exists (7.11), the program is ill-formed.
An xvalue of type
``\cvqual{cv1} \tcode{B}'' can be cast to type ``rvalue reference to
\cvqual{cv2} \tcode{D}'' with the same constraints as for an lvalue of
type ``\cvqual{cv1} \tcode{B}''. If the object
of type ``\cvqual{cv1} \tcode{B}'' is actually a base class subobject of an object
of type \tcode{D}, the result refers to the enclosing object of type
\tcode{D}. Otherwise, the behavior is undefined.
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [expr.static.cast] \P~11}
A prvalue of type ``pointer to \cvqual{cv1} \tcode{B}'', where \tcode{B}
is a class type, can be converted to a prvalue of type ``pointer to
\cvqual{cv2} \tcode{D}'', where \tcode{D} is a class derived (Clause 13)
from \tcode{B},
if \cvqual{cv2} is the same cv-qualification as,
or greater cv-qualification than, \cvqual{cv1}.
If \tcode{B} is a virtual base class of \tcode{D} or
a base class of a virtual base class of \tcode{D}, or
if no valid standard conversion from ``pointer to \tcode{D}''
to ``pointer to \tcode{B}'' exists (7.11), the program is ill-formed.
The null pointer value (7.11) is converted
to the null pointer value of the destination type. If the prvalue of type
``pointer to \cvqual{cv1} \tcode{B}'' points to a \tcode{B} that is
actually a subobject of an object of type \tcode{D}, the resulting
pointer points to the enclosing object of type \tcode{D}. Otherwise, the
behavior is undefined.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
struct A {
};

struct B : A {
  void fn() {
  }
};

void fn() {
  A a;
  B* ptr = static_cast<B*>(&a);  // undefined behavior, \tcode{a} is not an actual instance of \tcode{B}.
  B& ref = static_cast<B&>(a);   // undefined behavior for the same reason

  ptr->fn();
}
\end{codeblock}
\end{UBEntryExample}

\begin{UBEntryExample}
\begin{codeblock}
struct A {
};

struct B : A {
  void fn() {
  }
};

void fn(A& a) {
  B& b = static_cast<B&>(a); // OK (in this case), object passed to \tcode{fn} is an instance of \tcode{B}
  b.fn();
}

void fn2() {
  B b;
  fn(b);
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Values outside the range of the values of an enumeration}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.static.cast] \P~10}
A value of integral or enumeration type can be explicitly converted to
a complete enumeration type.
If the enumeration type has a fixed underlying type,
the value is first converted to that type
by integral conversion, if necessary, and
then to the enumeration type.
If the enumeration type does not have a fixed underlying type,
the value is unchanged
if the original value is within the range
of the enumeration values (10.2), and
otherwise, the behavior is undefined.
A value of floating-point type can also be explicitly converted to an enumeration type.
The resulting value is the same as converting the original value to the
underlying type of the enumeration (7.10), and subsequently to
the enumeration type.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
enum class some_enum : bool {
  A,
  B
};

void some_fn(some_enum se) {
  // \ldots{}
}

void some_other_fn() {
  // \ldots{}

  some_fn(static_cast<some_enum>(3)); // undefined behavior, \tcode{3} is out of range of the enumeration values

  // \ldots{}
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Converting a pointer to member of class type \tcode{A} to a pointer to member of class type \tcode{B}, where \tcode{B} is not a base or derived class of \tcode{A}}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.static.cast] \P~12}
A prvalue of type ``pointer to member of \tcode{D} of type \cvqual{cv1}
\tcode{T}'' can be converted to a prvalue of type ``pointer to member of
\tcode{B} of type \cvqual{cv2} \tcode{T}'', where \tcode{B} is a base
class (Clause 13) of \tcode{D},
if \cvqual{cv2} is the same cv-qualification
as, or greater cv-qualification than, \cvqual{cv1}.
If no valid standard conversion
from ``pointer to member of \tcode{B} of type \tcode{T}''
to ``pointer to member of \tcode{D} of type \tcode{T}''
exists (7.12), the program is ill-formed.
The null member pointer value (7.11) is converted to the null
member pointer value of the destination type. If class \tcode{B}
contains the original member, or is a base or derived class of the class
containing the original member, the resulting pointer to member points
to the original member. Otherwise, the behavior is undefined.
\begin{note}
Although class \tcode{B} need not contain the original member, the
dynamic type of the object with which indirection through the pointer
to member is performed must contain the original member;
see~8.5.
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
struct A {
  int a;
  int b;
};

struct B {
  char c;
};

struct C : A, B {
};

void fn() {
  int C::* c_ptr = &C::b;

  // Undefined behavior, member pointer \tcode{c_ptr} does not point to a member in \tcode{B}
  int B::* b_ptr = static_cast<int B::*>(c_ptr);

  // \ldots{}
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Casting away the \tcode{const} qualifier and attempting to write to an object}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.call] \P~7}
\begin{note}
A function can change the values of its non-const parameters, but these
changes cannot affect the values of the arguments except where a
parameter is of a reference type (11.3.2); if the reference is to
a const-qualified type, \tcode{const_cast} is required to be used to
cast away the constness in order to modify the argument's value. Where a
parameter is of \tcode{const} reference type a temporary object is
introduced if
needed~(10.1.7, 5.13, 5.13.5, 11.3.4, 15.2)
In addition, it is possible to modify the values of non-constant objects through
pointer parameters.
\end{note}
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [expr.const.cast] \P~6}
\begin{note}
Depending on the type of the object, a write operation through the
pointer, lvalue or pointer to data member resulting from a
\tcode{const_cast} that casts away a const-qualifier\textsuperscript{76}
may produce undefined behavior (10.1.7.1).
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
\begin{itemize}
\item Casting away \tcode{const} on a constant reference or pointer to const that
      refers to a non-const object of type \tcode{T} and modifying said object
      through the pointer or reference.
\end{itemize}
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
int i = 0;                         // Non-const \tcode{int}
const int& ref_i = i;
const_cast<int&>(ref_i) = 1;       // OK, modifies non-const \tcode{i}

const int j = 0;
const int& ref_j = j;
const_cast<int&>(ref_j) = 1;       // Undefined behavior, attempts to modify \tcode{j} which is \tcode{const}

int* ptr_j = const_cast<int*>(&j); 
*ptr_j = 2;                        // Undefined behavior, attempts to modify \tcode{j}
\end{codeblock}
\end{UBEntryExample}
\begin{UBEntryExample}
\begin{codeblock}
struct type {
  void fn(int v) const {
    const_cast<type*>(this)->i = v; // OK as long as \tcode{fn} isn't called on a \tcode{const} instance of \tcode{type}
  }

  int i = 3;
};

void fn() {
  type t1;
  t1.fn(4);  // OK

  const type t2;
  t2.fn(4);  // Undefined behavior
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Using a delete expression incorrectly}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.delete] \P~1}
The \grammarterm{delete-expression} operator destroys a most derived
object (4.5) or array created by a \grammarterm{new-expression}.

\begin{bnf}
delete-expression\br
    \terminal{::}\opt{} \terminal{delete} cast-expression\br
    \terminal{::}\opt{} \terminal{delete [ ]} cast-expression
\end{bnf}

The first alternative is for
non-array objects, and the second is for arrays.
Whenever the \tcode{delete} keyword is immediately
followed by empty square brackets, it shall be
interpreted as the second alternative.\textsuperscript{81}
The operand shall be of pointer to object type or of class type.
If of class type, the operand is contextually implicitly converted
(Clause 7) to a pointer to object type.\textsuperscript{82}
The \grammarterm{delete-expression}'s result has type \tcode{void}.
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} Footnote~82}
This implies that an object cannot be deleted using a pointer of type
\tcode{void*} because \tcode{void} is not an object type.
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [expr.delete] \P~2}
If the operand has a class type, the operand is converted to a pointer
type by calling the above-mentioned conversion function, and the
converted operand is used in place of the original operand for the
remainder of this section.
In the first alternative (\textit{delete object}), the value of
the operand of \tcode{delete} may be a null pointer value,
a pointer to a non-array object created by a previous
\grammarterm{new-expression}, or a pointer to a subobject (4.5)
representing a base class of such an object (Clause 13).
If not, the behavior is undefined.
In the second alternative (\textit{delete array}), the value of
the operand of \tcode{delete} may be a null pointer value or
a pointer value that resulted from a previous array
\grammarterm{new-expression}.\textsuperscript{83} If not, the
behavior is undefined.
\begin{note}
This means that the syntax of the \grammarterm{delete-expression}
must match the type of the object allocated by \tcode{new}, not the
syntax of the \grammarterm{new-expression}.
\end{note}
\begin{note}
A pointer to \tcode{const} type can be the operand of a
\grammarterm{delete-expression}; it is not necessary to cast away
the constness (8.2.11) of the pointer expression before it is
used as the operand of the \grammarterm{delete-expression}.
\end{note}
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [expr.delete]\P~3}
In the first alternative (\textit{delete object}), if the static type
of the object to be deleted is different from its dynamic type, the
static type shall be a base class of the dynamic type of the object
to be deleted and the static type shall have a virtual destructor
or the behavior is undefined. In the second alternative
(\textit{delete array}) if the dynamic type of the object to be
deleted differs from its static type, the behavior is undefined.
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [expr.delete] \P~5}
If the object being deleted has incomplete class type at the point
of deletion and the complete class has a non-trivial destructor or
a deallocation function, the behavior is undefined.
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [expr.delete] \P~11}
When a \grammarterm{delete-expression} is executed,
the selected deallocation function shall be called with
the address of the most-derived object in a \textit{delete object} case, or
the address of the object suitably adjusted for the array allocation
overhead (8.3.4) in the \textit{delete array} case,
as its first argument.
If a deallocation function
with a parameter of type \tcode{std::align_val_t}
is used,
the alignment of the type of the object to be deleted
is passed as the corresponding argument.
If a deallocation function
with a parameter of type \tcode{std::size_t} is used,
the size
of the most-derived type, or
of the array plus allocation overhead, respectively,
is passed as the corresponding argument.\textsuperscript{84}%
\begin{note}
If this results in a call to a usual deallocation function, and either
the first argument was not the result of
a prior call to a usual allocation function or
the second argument was not the corresponding argument in said call,
the behavior is undefined~(21.6.2.1, 21.6.2.2).
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
int* ptr = new int[10];

// \ldots{}

delete ptr; // Undefined behavior, \tcode{delete[]} must be used
\end{codeblock}
\end{UBEntryExample}

\begin{UBEntryExample}
\begin{codeblock}
int* ptr = new int;

// \ldots{}

delete[] ptr; // Undefined behavior, using array delete-expression on single object
\end{codeblock}
\end{UBEntryExample}

\begin{UBEntryExample}
\begin{codeblock}
void* ptr = new int;

// \ldots{}

delete ptr; // Undefined behavior, cannot use a \grammarterm{delete-expression} with a pointer to \tcode{void}
\end{codeblock}
\end{UBEntryExample}

\begin{UBEntryExample}
\begin{codeblock}
class Base {
public:
  virtual void fn() = 0;
};

class Derived : public Base {
  void fn() override {
    // \ldots{}
  }
};

void fn() {
  Base* ptr = new Derived;

  // \ldots{}

  delete ptr;  // Undefined behavior, \tcode{Base} is missing a virtual destructor
}
\end{codeblock}
\end{UBEntryExample}

\begin{UBEntryExample}
\begin{codeblock}
class Base {
public:
  virtual ~Base() = default;
  virtual void fn() = 0;
};

class Derived : public Base {
  void fn() override {
    // \ldots{}
  }
};

void fn() {
  A* ptr = new B[2];

  // \ldots{}

  delete[] ptr; // Undefined behavior, cannot delete an array of derived type
                // through a base class pointer
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Accessing nonexistent object members via pointer-to-member expressions}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.mptr.oper] \P~4}
Abbreviating \term{pm-expression}{}\tcode{.*}\term{cast-expression} as \tcode{E1.*E2}, \tcode{E1}
is called the \defn{object expression}.
If the dynamic type of \tcode{E1} does not
contain the member to which
\tcode{E2} refers, the behavior is undefined.
Otherwise, the expression \tcode{E1} is sequenced before the expression \tcode{E2}.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
struct Base {
  virtual ~Base() = default;
};

struct Derived : Base {
  void fn() {
    // \ldots{}
  }
};

void func1(Base* b) {
  auto ptr = static_cast<void(Base::*)()>(&Derived::fn);

  (b->*ptr)(); // Undefined behavior, invoking function through \tcode{Base} which doesn't contain \tcode{fn}
}

void func2(Base& b) {
  auto ptr = static_cast<void(Base::*)()>(&Derived::fn);

  (b.*ptr)(); // Nice try, this is also undefined behavior
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Indirecting through a null member pointer via a pointer-to-member operator}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.mptr.oper] \P~3}
The binary operator \tcode{->*} binds its second operand, which shall be
of type ``pointer to member of \tcode{T}'' to its first operand, which shall be of
type ``pointer to \tcode{U}''
where \tcode{U} is either \tcode{T} or
a class of which \tcode{T}
is an unambiguous and accessible base class.
The expression \tcode{E1->*E2} is converted into the equivalent form
\tcode{(*(E1)).*E2}.
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [expr.mptr.oper] \P~6}
If the result of \tcode{.*} or \tcode{->*} is a function, then that
result can be used only as the operand for the function call operator
\tcode{()}.
\begin{example}

\begin{codeblock}
(ptr_to_obj->*ptr_to_mfct)(10);
\end{codeblock}

calls the member function denoted by \tcode{ptr_to_mfct} for the object
pointed to by \tcode{ptr_to_obj}.
\end{example}

In a \tcode{.*} expression whose object expression is an rvalue, the program is
ill-formed if the second operand is a pointer to member function
with \grammarterm{ref-qualifier} \tcode{\&}.
In a \tcode{.*}
expression whose object expression is an lvalue, the program is ill-formed if the second
operand is
a pointer to member function
with \grammarterm{ref-qualifier} \tcode{\&\&}.
The result of a \tcode{.*} expression
whose second operand is a pointer to a data member is an lvalue if the first
operand is an lvalue and an xvalue otherwise. The result of a \tcode{.*} expression whose
second operand is a pointer to a member function is a prvalue.
If the second operand is the null
member pointer value (7.12), the behavior is undefined.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
struct A {
  int data;
};

void fn1(A* a) {
  int A::* ptr = nullptr;

  // \ldots{}

  a->*ptr = 3; // Undefined behavior, dereferencing null member pointer 
}

void fn2(A& a) {
  int A::* ptr = nullptr

  // \ldots{}

  a.*ptr = 3; // Also undefined behavior
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Using zero as a divisor in a division or modulo expression}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.mul] \P~4}
The binary \tcode{/} operator yields the quotient, and the binary
\tcode{\%} operator yields the remainder from the division of the first
expression by the second.
If the second operand of \tcode{/} or \tcode{\%} is zero the behavior is
undefined.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
const int result1 = 1 / 0; // Undefined behavior

const int result2 = 1 % 0; // Undefined behavior
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{A division or modulo operation's output value not being representable in the type of the result}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.mul] \P~4}
\ldots{}

For integral operands the \tcode{/} operator yields the algebraic quotient with
any fractional part discarded;\textsuperscript{85}
if the quotient \tcode{a/b} is representable in the type of the result,
\tcode{(a/b)*b + a\%b} is equal to \tcode{a}; otherwise, the behavior
of both \tcode{a/b} and \tcode{a\%b} is undefined.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
// Undefined behavior on a two's complement system
const int result = std::numeric_limits<int>::min() % -1;
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{TODO: This needs a concise name}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.add] \P~4}
When an expression that has integral type is added to or subtracted from
a pointer, the result has the type of the pointer operand.
If the expression \tcode{P} points to element $\mathtt{x[}i\mathtt{]}$
of an array object \tcode{x} with $n$ elements,\textsuperscript{86}
the expressions \tcode{P + J} and \tcode{J + P}
(where \tcode{J} has the value $j$)
point to the (possibly-hypothetical) element
$\mathtt{x[}i + j\mathtt{]}$ if $0 \le i + j \le n$;
otherwise, the behavior is undefined.
Likewise, the expression \tcode{P - J}
points to the (possibly-hypothetical) element
$\mathtt{x[}i - j\mathtt{]}$ if $0 \le i - j \le n$;
otherwise, the behavior is undefined.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
void fn() {
  int arr[8] = {};

  // \ldots{}

  int* offset = arr + 9; // undefined behavior, outside the range $0 \le i + j \le 8$
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Subtracting two pointers to elements in an array where both operands aren't pointing to elements in the same array}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.add] \P~5}
When two pointers to elements of the same array object are subtracted,
the type of the result is an implementation-defined signed
integral type; this type shall be the same type that is defined as
\tcode{std::ptrdiff_t} in the \tcode{<cstddef>}
header (21.2).
If the expressions \tcode{P} and \tcode{Q}
point to, respectively,
elements
$\mathtt{x[}i\mathtt{]}$
and
$\mathtt{x[}j\mathtt{]}$
of the same array object \tcode{x},
the expression \tcode{P - Q} has the value $i - j$;
otherwise, the behavior is undefined.
\begin{note}
If the value $i - j$
is not in the range of representable values
of type \tcode{std::ptrdiff_t},
the behavior is undefined.
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
void fn() {
  int arr1[2] = {};
  int arr2[2] = {};

  std::ptrdiff_t result = &arr1[0] - &arr2[1]; // Undefined behavior
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Performing pointer addition or subtraction on a pointer to type \cv{}~\tcode{T} where \tcode{T} and the array element type are not similar}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.add] \P~6}
For addition or subtraction, if the expressions \tcode{P} or \tcode{Q} have
type ``pointer to \cv{}~\tcode{T}'', where \tcode{T} and the array element type
are not similar (7.5), the behavior is undefined.
\begin{note} In particular, a pointer to a base class cannot be used for
pointer arithmetic when the array contains objects of a derived class type.
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
#include <cstddef>
#include <iterator>

struct A {
  int count = 0;
};
struct B : A {
  long long other_count = 0;
};

void fn(const A* base, std::size_t size) {
  // Undefined behavior, using pointer addition with pointer to base class
  // over array containing a derived type
  const A* end = base + size;

  // \ldots{}
}

int main() {
  B data[5];
  fn(data, std::size(data));
  return 0;
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Using a negative right operand with a shift operator}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.shift] \P~1}
The shift operators \tcode{<<} and \tcode{>>} group left-to-right.
%
\begin{bnf}
shift-expression\br
    additive-expression\br
    shift-expression \terminal{<<} additive-expression\br
    shift-expression \terminal{>>} additive-expression
\end{bnf}

The operands shall be of integral or unscoped enumeration type and integral
promotions are performed. The type of the result is that of the promoted
left operand.
The behavior is undefined if the right operand is negative, or greater
than or equal to the length in bits of the promoted left operand.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
int fn1(std::int32_t value) {
  return value << -1; // Undefined behavior, negative right operand
}

int fn2(std::int32_t value) {
  return value >> -1; // Undefined behavior
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{A shift amount in a shift operation being greater than or equal to the length in bits of the promoted left operand's type}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.shift] \P~1}
\ldots{}

The behavior is undefined if the right operand is negative, or greater
than or equal to the length in bits of the promoted left operand.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
int fn(std::int32_t value) {
  // Undefined behavior, shift amount equal to number of bits in \tcode{std::int32_t}.
  return value << 32;
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Left-shifting a negative value}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.shift] \P~2}
The value of \tcode{E1 << E2} is \tcode{E1} left-shifted \tcode{E2} bit positions; vacated bits are
zero-filled. If \tcode{E1} has an unsigned type, the value of the result
is $\mathrm{E1}\times2^\mathrm{E2}$, reduced modulo
one more than the maximum value representable in the result type. Otherwise, if
\tcode{E1} has a signed type and non-negative value, and $\mathrm{E1}\times2^\mathrm{E2}$ is
representable in the corresponding unsigned type of the result type, then
that value, converted to the result type, is the resulting value; otherwise, the
behavior is undefined.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
int fn() {
  return -1 << 4; // Undefined behavior, left-shifting a negative value.
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{TODO: This needs a concise name}

\UBEntryStatusTable{\CppXVIIShort{} [TODO: Add the full range]}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [expr.ass] \P~8}
If the value being stored in an object is read via another object that
overlaps in any way the storage of the first object, then the overlap shall be
exact and the two objects shall have the same type, otherwise the behavior is
undefined. \begin{note} This restriction applies to the relationship
between the left and right sides of the assignment operation; it is not a
statement about how the target of the assignment may be aliased in general.
See~(6.10). \end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
TODO
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}
