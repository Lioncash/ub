\chapter{General principles}

\begin{UBEntrySection}{Causing an unsequenced side effect relative to another side effect on the same memory location or a value computation using the value of any object in the same memory location, and they aren't potentially concurrent}

\UBEntryStatusTable{\CppXCVIIIShort--\CppXXShort}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [intro.execution] \P~17}
Except where noted, evaluations of operands of individual operators and
of subexpressions of individual expressions are unsequenced.
\begin{note}
In an expression that is evaluated more than once during the execution of a program,
unsequenced and indeterminately sequenced evaluations of
its subexpressions need not be performed consistently in different evaluations.
\end{note}
The value computations of the operands of an operator are sequenced before
the value computation of the result of the operator. If a side effect on a
memory location~(4.4) is unsequenced relative to either another side effect
on the same memory location or a value computation using the value of any object
in the same memory location, and they are not potentially concurrent~(4.7),
the behavior is undefined.
\begin{note}
The next section imposes similar, but more complex restrictions on
potentially concurrent computations.
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None.
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
void g(int i) {
  i = 7, i++, i++; // \tcode{i} becomes \tcode{9}
  i = i++ + 1;     // the value of \tcode{i} is incremented
  i = i++ + i;     // the behavior is undefined
  i = i + 1;       // the value of \tcode{i} is incremented
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Using the name of a subobject to access a new object if the original subobject contains a reference or \tcode{const} subobject}

\UBEntryStatusTable{\CppXCVIIIShort--\CppXXShort}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [intro.object] \P~2}
Objects can contain other objects, called \defnx{subobjects}.
A subobject can be a \defn{member subobject}~(12.2), a \defn{base class subobject}~(Clause 13), or an array element.
An object that is not a subobject of any other object is called a \defn{complete object}.
If an object is created in storage associated with a member subobject or array element \placeholder{e}
(which may or may not be within its lifetime),
the created object is a subobject of \placeholder{e}'s containing object if:
\begin{itemize}
\item[---]
the lifetime of \placeholder{e}'s containing object has begun and not ended, and
\item[---]
the storage for the new object exactly overlays the storage location associated with \placeholder{e}, and
\item[---]
the new object is of the same type as \placeholder{e} (ignoring cv-qualification).
\end{itemize}
\begin{note}
If the subobject contains a reference member or a \tcode{const} subobject,
the name of the original subobject cannot be used to access the new object~(6.8)
\end{note}
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
\begin{UBEntryException}
\tcode{std::launder} may be used to obtain a pointer to an object created in storage occupied by an
existing object of the same type, even if it contains references or \tcode{const} subobjects.
\end{UBEntryException}
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
struct X { const int n; };
union U { X x; float f; };
void tong() {
  U u = {{ 1 }};
  u.f = 5.f;                          // OK, creates new subobject of \tcode{u}
  X *p = new (&u.x) X {2};            // OK, creates new subobject of \tcode{u}
  assert(p->n == 2);                  // OK
  assert(*std::launder(&u.x.n) == 2); // OK
  assert(u.x.n == 2);                 // undefined behavior, \tcode{u.x} does not name new subobject
}
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Attempting to modify a \tcode{const} object}

\UBEntryStatusTable{\CppXCVIIIShort--\CppXXShort}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [intro.execution] \P~4}
Certain other operations are described in this International Standard as undefined
(for example, the effect of attempting to modify a \tcode{const} object).
\begin{note}
This International Standard imposes no requirements on the behavior of programs that contain undefined behavior.
\end{note}
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [dcl.type.cv] \P~4}
Except that any class member declared \tcode{mutable}~(10.1.1)
can be modified, any attempt to modify a \tcode{const} object during its
lifetime~(6.8) results in undefined behavior.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
\begin{UBEntryException}
Class data members that have the \tcode{mutable} specifier applied to them.
\end{UBEntryException}
\end{UBEntryExceptions}

\begin{UBEntryExceptionQuotes}
\begin{StdQuote}{\CppXVIIShort{} [dcl.stc] \P~8}
The \tcode{mutable} specifier on a class data member nullifies a
\tcode{const} specifier applied to the containing class object and
permits modification of the mutable class member even though the rest
of the object is \tcode{const}~(10.1.7.1).
\end{StdQuote}
\end{UBEntryExceptionQuotes}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
const int ci = 3;                   // cv-qualified (initialized as required)
ci = 4;                             // ill-formed: attempt to modify \tcode{const}

int i = 2;                          // not cv-qualified
const int* cip;                     // pointer to \tcode{const int}
cip = &i;                           // OK: cv-qualified access path to unqualified
*cip = 4;                           // ill-formed: attempt to modify through ptr to \tcode{const}

int* ip;
ip = const_cast<int*>(cip);         // cast needed to convert \tcode{const int*} to \tcode{int*}
*ip = 4;                            // defined: \tcode{*ip} points to \tcode{i}, a non-\tcode{const} object

const int* ciq = new const int (3); // initialized as required
int* iq = const_cast<int*>(ciq);    // cast required
*iq = 4;                            // undefined: modifies a \tcode{const} object
\end{codeblock}
\end{UBEntryExample}

\begin{UBEntryExample}
\begin{codeblock}
struct X {
  mutable int i;
  int j;
};
struct Y {
  X x;
  Y();
};

const Y y;
y.x.i++;                   // well-formed: \tcode{mutable} member can be modified
y.x.j++;                   // ill-formed: const-qualified member modified
Y* p = const_cast<Y*>(&y); // cast away const-ness of \tcode{y}
p->x.i = 99;               // well-formed: \tcode{mutable} member can be modified
p->x.j = 99;               // undefined: modifies a \tcode{const} member
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}

\begin{UBEntrySection}{Causing a data race}

\UBEntryStatusTable{\CppXCVIIIShort--\CppXXShort}{None}

\begin{UBEntryQuotes}
\begin{StdQuote}{\CppXVIIShort{} [intro.races] \P~1}
The value of an object visible to a thread \placeholder{T} at a particular point is the
initial value of the object, a value assigned to the object by \placeholder{T}, or a
value assigned to the object by another thread, according to the rules below.
\begin{note} In some cases, there may instead be undefined behavior. Much of this
section is motivated by the desire to support atomic operations with explicit
and detailed visibility constraints. However, it also implicitly supports a
simpler view for more restricted programs. \end{note}
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [intro.races] \P~2}
Two expression evaluations \defn{conflict} if one of them modifies a memory
location (4.4) and the other one reads or modifies the same memory location.
\end{StdQuote}

\begin{StdQuote}{\CppXVIIShort{} [intro.races] \P~20}
Two actions are \defn{potentially concurrent} if
\begin{itemize}
\item[---] they are performed by different threads, or
\item[---] they are unsequenced, at least one is performed by a signal handler, and they are not both performed by the same signal handler invocation.
\end{itemize}
The execution of a program contains a \defn{data race} if it contains two
potentially concurrent conflicting actions, at least one of which is not atomic,
and neither happens before the other, except for the special case for signal handlers
described below. Any such data race results in undefined behavior.
\end{StdQuote}
\end{UBEntryQuotes}

\begin{UBEntryExceptions}
None
\end{UBEntryExceptions}

\begin{UBEntryExamples}
\begin{UBEntryExample}
\begin{codeblock}
int counter = 0;
auto fn = [&]{ counter++; };
std::thread t1{fn}, t2{fn};  // Undefined behavior
\end{codeblock}
\end{UBEntryExample}

\begin{UBEntryExample}
\begin{codeblock}
volatile int counter = 0;    // \tcode{volatile} does \textbf{not} provide any thread-safety guarantees
auto fn = [&]{ counter++; };
std::thread t1{fn}, t2{fn};  // Undefined behavior
\end{codeblock}
\end{UBEntryExample}

\begin{UBEntryExample}
\begin{codeblock}
std::atomic<int> counter{0};
auto fn = [&]{ counter++; };
std::thread t1{fn}, t2{fn};  // OK
\end{codeblock}
\end{UBEntryExample}
\end{UBEntryExamples}
\end{UBEntrySection}
